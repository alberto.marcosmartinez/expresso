﻿using Expesso.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Expesso.Services
{
    public class ApiServices
    {
        public async Task<List<Menu>> GetMenu()
        {
            var client = new HttpClient();
            var result = await client.GetStringAsync("https://expressoapp.azurewebsites.net/api/menus");
            return JsonConvert.DeserializeObject<List<Menu>>(result);
        }

        public async Task<bool> ReserveTable(Reservations reservation)
        {
            try
            {
                var client = new HttpClient();
                var json = JsonConvert.SerializeObject(reservation);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync("http://expressoapp.azurewebsites.net/api/reservations", content);
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;               
            }
        }
    }
}
