﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Expesso.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InfoPage : ContentPage
	{
		public InfoPage ()
		{
			InitializeComponent ();
		}

        private void TapFacebook_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://wwww.facebook.com/Expresso-2291246571109615"));
        }

        private void TapTwitter_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://wwww.facebook.com/Expresso-2291246571109615"));
        }

        private void TapInstagram_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://wwww.facebook.com/Expresso-2291246571109615"));
        }

        private void TapYouTube_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://wwww.facebook.com/Expresso-2291246571109615"));
        }

        private void TapCall_Tapped(object sender, EventArgs e)
        {
            PhoneDialer.Open("+34 647946052");
        }
    }
}