﻿using Expesso.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Menu = Expesso.Models.Menu;

namespace Expesso.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubMenuPage : ContentPage
	{
        public ObservableCollection<SubMenu> SubMenus;

		public SubMenuPage (Menu menu)
		{
			InitializeComponent ();
            SubMenus = new ObservableCollection<SubMenu>();
            foreach (var subMenu in menu.SubMenus)
            {
                SubMenus.Add(subMenu);
            }
            lvSubMenu.ItemsSource = SubMenus;
		}
	}
}