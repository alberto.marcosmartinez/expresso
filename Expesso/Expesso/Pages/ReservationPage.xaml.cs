﻿using Expesso.Models;
using Expesso.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Expesso.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReservationPage : ContentPage
	{
		public ReservationPage ()
		{
			InitializeComponent ();
		}

        private async void BtnBookTable_Clicked(object sender, EventArgs e)
        {
            Reservations reservation = new Reservations()
            {
                Name = EntName.Text,
                Email = EntEmail.Text,
                Phone = EntPhone.Text,
                TotalPeople = EntTotalPeople.Text,
                Date = DpBookingDate.Date.ToString(),
                Time = TpBookingTime.Time.ToString()
            };

            ApiServices apiService = new ApiServices();
            var result = await apiService.ReserveTable(reservation);

            if (result != true)
            {
                await DisplayAlert("Opps", "Something goes wrong", "Allright");
            }
            else
            {
                await DisplayAlert("Ok", "Your table has been reserved successfully", "Allright");
            }
        }
    }
}